

## Setup

The following steps need to be followed before the scripts will work.
Any mention of $SOMETHING is a value you will have to adapt and specify accordingly in the config-files.
The $-sign is simply an indication, so "$DOMAIN" should be replaced by "domain.com" and NOT by "$domain.com".
Some things *must* be changed, such as your server-domain, some things simply *should* be changed, because it's good practice or security related.

### Set up your remote machine

It's bad practice to ssh into a remote server as root for the purpose of automated backups.
On the other hand, when we back up the files of a tool, we want to keep permissions consistent for an easy restore of the backup.
We set up a new user on the remote machine, give them sudo-rights w/o password prompt and feed them our ssh-keys.

```
$ ssh root@$YOUR_SERVER  # or physical access, if possible
# adduser $BACKUP_USER
```

Now add your new user to the sudoers and specify that you don't need a password, by adding the following line after
starting visudo:

```
# visudo
```
and add this line where it feels appropriate:
```
$BACKUP_USER    ALL =(ALL) NOPASSWD:ALL
```

You'll have to log in again as `$BACKUP_USER` for this to have effect.

Finally, ssh-login as `$BACKUP_USER` and create a `.ssh` directory in the home of `$BACKUP_USER`.

```
$ ssh $BACKUP_USER@YOUR_SERVER
$ mkdir ~/.ssh
```

### Set up your local machine

Mainly, you want to create ssh-keys and add them to the remote server, e.g.:

[Tutorial](https://adamdehaven.com/blog/how-to-generate-als -al ~/.sshn-ssh-key-and-add-your-public-key-to-the-server-for-authentication/)

```

(... todo ...)
```

### Fill out the config files with corresponding information

(... todo: explain healthchecks, etc. ...)

