#!/bin/bash

# Variables are sourced from a config-file

# Load utils with shared functions
. "$(dirname "${BASH_SOURCE[0]}")"/../util.sh

backup_dokuwiki () {

  echo "Backup dokuwiki directory..."
  incremental_rsync "$SSH_USER@$SERVER_URL:$DOKUWIKI_INSTALL_PATH" "$BACKUP_DIR"
  echo "Backup apache2 directory..."
  incremental_rsync "$SSH_USER@$SERVER_URL:/etc/apache2" "$BACKUP_DIR"
  echo "Backup letsencrypt directory"
  incremental_rsync "$SSH_USER@$SERVER_URL:/etc/letsencrypt" "$BACKUP_DIR"

  delete_old_backups $BACKUP_DIR

  healthcheck "$HEALTHCHECK_URL"

  echo "Done"
}


# The following lines define the behaviour of the command line interface
if [ "$1" = "--config" ] && ! [ -z "$2" ]; then
  # Read config-file
  . "$2"

  # Check if backup-dir exists
  if [ -d "$BACKUP_DIR" ]
  then
    backup_dokuwiki
  else
    echo "Backup directory $BACKUP_DIR not mounted / empty"
  fi
else
  echo "Usage: call with path to config-file, e.g. --config /path/to/file.config"
fi
