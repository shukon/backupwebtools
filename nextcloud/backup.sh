#!/bin/bash

# Inspired from https://codeberg.org/DecaTec/Nextcloud-Backup-Restore
# This script implements a minimal / tweaked version for a specific Nextcloud Setup.
# Variables are sourced from a config-file

# Load utils with shared functions
. "$(dirname "${BASH_SOURCE[0]}")"/../util.sh

backup_nextcloud () {

  # This is the name in the backup. Can be anything but needs to be consistent with restore.sh
  fileNameBackupDb='nextcloud-db.sql'
  
  echo "Set maintenance mode for Nextcloud..."
  ssh $SSH_USER@$SERVER_URL "sudo -u "${webserverUser}" php ${nextcloudFileDir}/occ maintenance:mode --on"

  echo "Backup file directory..."
  incremental_rsync "$SSH_USER@$SERVER_URL:$nextcloudFileDir" "$BACKUP_DIR"
  echo "Backup data directory..."
  incremental_rsync "$SSH_USER@$SERVER_URL:$nextcloudDataDir" "$BACKUP_DIR"
  echo "Dump Nextcloud database (MySQL/MariaDB!) to /tmp and get it with rsync..."
  ssh $SSH_USER@$SERVER_URL "rm /tmp/${fileNameBackupDb}"
  ssh $SSH_USER@$SERVER_URL "mysqldump --single-transaction -h localhost -u ${dbUser} -p${dbPassword} ${nextcloudDatabase} > /tmp/${fileNameBackupDb}"
  incremental_rsync "$SSH_USER@$SERVER_URL:/tmp/${fileNameBackupDb}" "$BACKUP_DIR"
  echo "Backup apache2 directory..."
  incremental_rsync "$SSH_USER@$SERVER_URL:/etc/apache2" "$BACKUP_DIR"
  echo "Backup letsencrypt directory"
  incremental_rsync "$SSH_USER@$SERVER_URL:/etc/letsencrypt" "$BACKUP_DIR"

  echo "Switching off maintenance mode..."
  ssh $SSH_USER@$SERVER_URL "sudo -u "${webserverUser}" php ${nextcloudFileDir}/occ maintenance:mode --off"

  delete_old_backups $BACKUP_DIR

  healthcheck "$HEALTHCHECK_URL"

  echo "Done"
}


# The following lines define the behaviour of the command line interface
if [ "$1" = "--config" ] && ! [ -z "$2" ]; then
  # Read config-file
  . "$2"

  # Check if backup-dir exists
  if [ -d "$BACKUP_DIR" ]
  then
    backup_nextcloud
  else
    echo "Backup directory $BACKUP_DIR not mounted / empty"
  fi
else
  echo "Usage: call with path to config-file, e.g. --config /path/to/file.config"
fi
