#!/bin/bash

# Load utils with shared functions
. "$(dirname "${BASH_SOURCE[0]}")"/util.sh

BASE_DIR="$(dirname "${BASH_SOURCE[0]}")"

$BASE_DIR/dokuwiki/backup.sh --config $BASE_DIR/dokuwiki/dokuwiki.config
$BASE_DIR/nextcloud/backup.sh --config $BASE_DIR/nextcloud/nextcloud.config
$BASE_DIR/rocketchat/backup.sh --config $BASE_DIR/rocketchat/rocketchat.config
#$BASE_DIR/keycloak/backup.sh --config $BASE_DIR/keycloak/keycloak.config
#$BASE_DIR/moodle/backup.sh --config $BASE_DIR/moodle/moodle.config

# Back up the backups ;-)

backup_the_backups () {
  # Backup the main-harddrive to the backup-harddrive
  . $BASE_DIR/main.config
  rsync -aXAHv --info=progress2 --delete --force --exclude README_BACKUP_BDP.txt $MAIN/ $MAIN_BACKUP
  healthcheck "$HEALTHCHECK_URL"
}

backup_the_backups
