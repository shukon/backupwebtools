#!/bin/bash

# This file provides necessary common functions for all backup-routines

incremental_rsync () {
  # First check usage and echo appropriate error
  if [ $# -ne 2 ] || [ -z "$1" ] || [ -z "$2" ]
  then
    echo "Usage: incremental_rsync FROM TO"
    return 1
  fi

  # Folder name convention for backups throughout these scripts is YYYYMMDD
  today=$(date '+%Y%m%d')  # e.g. 20200501

  # perform rsync backup, use hardlinks for incremental backups
  # compare (for hardlinks) to newest backup
  # first, find newest/latest backup by iterating over folders and find last matching regex
  latest=""
  for entry in "$2"/*
  do
    echo "Found $entry"
    [[ $(basename "$entry") =~ ^[0-9]{8}$ ]] && [[ $(basename "$entry") != "$today" ]] && latest="$entry"
  done
  echo "Comparing today ($today) against latest backup: $latest"

  if rsync -aXAHv --info=progress2 --delete --link-dest="$latest" "$1" "$2/$today"; then
    echo "Successfully transfered \"$1\" to \"$2\""
  else
    echo "Failed transfering \"$1\" to \"$2\""
    return 1
  fi
}

delete_old_backups () {
  # expects one argument, decides which backups to keep
  # $1 is string of folder in the format of YYYYMMDD

  # Define here which ones to keep
  declare -a keep=(
      $(date '+%Y%m%d')                     # today
      $(date -d "1 day ago" '+%Y%m%d')      # yesterday
      $(date -d "2 day ago" '+%Y%m%d')      # before  yesterday
      $(date -d 'last Monday' '+%Y%m%d')    # last monday
      $(date '+%Y%m')"01"                   # first of this month
      $(date -d '1 month ago' '+%Y%m')"01"  # first of last month
      $(date -d '6 month ago' '+%Y')"0601"  # first of june
      $(date '+%Y')"0101"                   # new year
      )

  for entry in "$1"/*
  do
    base=$(basename $entry)
    if [[ " ${keep[@]} " =~ " ${base} " ]] ||\
       ! [[ "$base" =~ ^[0-9]{8}$ ]]; then
	   echo "Keeping: $base($entry)";
    else
      echo "Delete: $entry";
      rm $entry -rf
    fi
  done
}

healthcheck () {
  # Perform healthcheck on successful updates
  curl -fsS --retry 3 $1
  # Check free space:
  # pct=`df --output=pcent / | tail -n 1 | tr -d '% '`
  # if [ $pct -gt 75 ]; then url=$1/fail; fi
  # curl -fsS --retry 3 -X POST --data-raw "Used space on /: $pct%" $1
}
