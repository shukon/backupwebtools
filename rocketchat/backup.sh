#!/bin/bash

# Variables are sourced from a config-file

# Load utils with shared functions
. "$(dirname "${BASH_SOURCE[0]}")"/../util.sh

backup_rocketchat () {
  echo "Backup Rocket.Chat..."

  echo "Halt Rocket.Chat"
  ssh $SSH_USER@$SERVER_URL "systemctl stop rocketchat.service"

  echo "Dump database"
  ssh $SSH_USER@$SERVER_URL "rm /tmp/mongodb_dump/ -r"
  ssh $SSH_USER@$SERVER_URL "mongodump --out=/tmp/mongodb_dump/"
  incremental_rsync "$SSH_USER@$SERVER_URL:/tmp/mongodb_dump" "$BACKUP_DIR"

  echo "Copy data-folder"
  incremental_rsync "$SSH_USER@$SERVER_URL:/opt/Rocket.Chat" "$BACKUP_DIR"

  echo "Backup apache2 directory..."
  incremental_rsync "$SSH_USER@$SERVER_URL:/etc/apache2" "$BACKUP_DIR"

  echo "Backup letsencrypt directory"
  incremental_rsync "$SSH_USER@$SERVER_URL:/etc/letsencrypt" "$BACKUP_DIR"

  echo "Restart Rocket.Chat"
  ssh $SSH_USER@$SERVER_URL "systemctl start rocketchat.service"

  delete_old_backups $BACKUP_DIR

  healthcheck "$HEALTHCHECK_URL"

  echo "Done"
}


# The following lines define the behaviour of the command line interface
if [ "$1" = "--config" ] && ! [ -z "$2" ]; then
  # Read config-file
  . "$2"

  # Check if backup-dir exists
  if [ -d "$BACKUP_DIR" ]
  then
    backup_rocketchat
  else
    echo "Backup directory $BACKUP_DIR not mounted / empty"
  fi
else
  echo "Usage: call with path to config-file, e.g. --config /path/to/file.config"
fi
